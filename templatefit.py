from math import sqrt, hypot
import ctypes
import ROOT
from ROOT import RooFit, RooRealVar, RooArgSet, RooArgList, RooDataHist, RooHistPdf, RooRealVar, RooAddPdf, RooPolynomial, RooMsgService

def fillHistFromList(values, xbins, name, makeint = False):
  if len(values) != len(xbins)-1:
    raise Exception('templatefit.py :: fillHistFromList() :: len(values) != len(xbins)-1')

  hist = ROOT.TH1D(name, name, len(xbins)-1, xbins)
  entries = 0
  for i in range(len(values)):
    value = int(values[i]) if makeint else values[i]
    hist.SetBinContent(i+1, value)
    entries += value

  hist.SetEntries(entries)
  hist.Sumw2()

  #  n = 0
  #  while True:
  #    if n == values[i]:
  #      break
  #    n += 1
  #    hist.Fill(xbins[i], 1)

  return hist


def getEffNadezda(num, den, xbins, name):
  eff = ROOT.TGraphAsymmErrors(len(xbins)-1)
  eff.SetName(name)
  eff.SetTitle(name)

  for i in range(len(xbins)-1):
    teff = ROOT.TEfficiency(name+"temp"+str(i), name+"temp"+str(i), 1, 0 ,1)
    teff.SetTotalEvents(1, int(den[i]))
    teff.SetPassedEvents(1, int(num[i]))

    x = xbins[i] + (xbins[i+1]-xbins[i])/2
    ex = (xbins[i+1]-xbins[i])/2
    eyl = teff.GetEfficiencyErrorLow(1)
    eyh = teff.GetEfficiencyErrorUp(1)
    eff.SetPoint(i, x, teff.GetEfficiency(1))
    eff.SetPointError(i, ex, ex, eyl, eyh)
  
  return eff

def getEff(num, den, xbins, name):
  hnum = fillHistFromList(num, xbins, "num_"+name, makeint = True)
  hden = fillHistFromList(den, xbins, "den_"+name, makeint = True)

  eff = ROOT.TGraphAsymmErrors(hnum, hden, "cl=0.683 b(1,1) mode")
  eff.SetName(name)
  eff.SetTitle(name)
  
  return eff

def getIntegralAndError(hist, nlow, nhigh):
  blow = hist.FindBin(nlow)
  bhigh = hist.FindBin(nhigh)-1

  cerror = ctypes.c_double(0)
  hsum = hist.IntegralAndError(blow, bhigh, cerror)
  herror = cerror.value

  return hsum, herror

def scaleIntegralToPoissonError(num, den, nlow, nhigh):

  numsum, numerror = getIntegralAndError(num, nlow, nhigh)
  densum, denerror = getIntegralAndError(den, nlow, nhigh)

  if densum == 0:
    return 0, 0

  den_scale = pow(densum/denerror,2)/densum
  num_scale = 0

  if numsum != 0:
    num_scale = pow(numsum/numerror,2)/numsum

  ratio = num_scale/den_scale
  if ratio < 0.85 or 1.15 < ratio:
    print "Poisson scale factor difference is large:",ratio

  # return int(numsum*den_scale), int(densum*den_scale)
  return numsum, densum

def fixEmptyBins(hist):
  fixed = hist.Clone()
  fixed.SetName(hist.GetName()+"_fixed")
  fixed.SetTitle(hist.GetTitle()+"_fixed")

  minval = hist.GetMinimum(0)

  for b in range(1,hist.GetNbinsX()+1):
    if fixed.GetBinContent(b) < minval/2:
      fixed.SetBinContent(b, minval/100)

  return fixed

def fitData(data, sig, bkg = None, xlow = -1, xhigh = -1, nlow = -1, nhigh = -1, plotname = ""):
  # xlow/xhigh are the fit range, default: full range
  # nlow/nhigh are the integral range to determine number of real photons, default: full range

  RooMsgService.instance().setGlobalKillBelow(RooFit.WARNING);

  mlow  = data.GetXaxis().GetBinLowEdge(1)
  mhigh = data.GetXaxis().GetBinLowEdge(data.GetNbinsX()+1)

  if xlow == -1:  xlow = mlow
  if xhigh == -1: xhigh = mhigh

  if xlow == -1:  nlow = mlow
  if xhigh == -1: nhigh = mhigh

  blow = data.GetXaxis().FindBin(nlow);
  bhigh = data.GetXaxis().FindBin(nhigh)-1;
  ndata_eff = data.Integral(blow,bhigh)

  blow = data.GetXaxis().FindBin(xlow);
  bhigh = data.GetXaxis().FindBin(xhigh)-1;
  ndata_fit = data.Integral(blow,bhigh)

  mass = RooRealVar("mass","m_llg", mlow, mhigh)
  mass.setRange("fit_range", xlow, xhigh)
  mass.setRange("full_range", mlow, mhigh)
  mass.setRange("eff_range", nlow, nhigh)
  mass.setRange("low_range", mlow, nlow)
  mass.setRange("high_range", nhigh, mhigh)

  arg = RooArgList(mass)
  var = RooArgSet(mass)

  hdata = RooDataHist("data", "data", arg, data)
  datapdf = RooHistPdf("datapdf", "datapdf", var, hdata)

  hsig   = RooDataHist("sig" , "sig" , arg, sig)
  sigpdf = RooHistPdf("sigpdf", "sigpdf", var, hsig)

  bkgpdf = None

  if bkg == None:
    # Use 3rd order poly as background, if there's enough data for fit
    full = datapdf.createIntegral(var, RooFit.Range("full_range"))
    bands = datapdf.createIntegral(var, RooFit.Range("low_range,high_range"))
    print "***** datapdf full = ", full.getVal(), ", bands = ", bands.getVal(), "ratio = ", bands.getVal()/full.getVal()
    if bands.getVal()/(bands.getVal() + full.getVal()) < 0.05:
      return ndata_eff

    p1 = RooRealVar("p1","p1", 0, -20, 20)
    p2 = RooRealVar("p2","p2", 0, -20, 20)
    p3 = RooRealVar("p3","p3", 0, -20, 20)

    bkgpdf = RooPolynomial("bkgpdf", "bkgpdf", mass, RooArgList(p1,p2,p3))
  else:
    # Use MC template as background, if there's enough data for fit
    # print "Number of background entries:", bkg.GetEntries()
    # print "Fraction background weights (pre):", bkg.GetSumOfWeights()/(bkg.GetSumOfWeights()+sig.GetSumOfWeights())
    # full = datapdf.createIntegral(var, RooFit.Range("eff_range"))
    # bands = datapdf.createIntegral(var, RooFit.Range("low_range,high_range"))
    # print "datapdf eff = ", full.getVal(), ", bands = ", bands.getVal(), "ratio = ", bands.getVal()/full.getVal()
    if bkg.GetEntries() < 40:
      return ndata_eff

    bkg = fixEmptyBins(bkg)

    hbkg   = RooDataHist("bkg" , "bkg" , arg, bkg)
    bkgpdf = RooHistPdf("bkgpdf", "bkgpdf", var, hbkg)

    # full = bkgpdf.createIntegral(var, RooFit.Range("eff_range"))
    # fit = bkgpdf.createIntegral(var, RooFit.Range("fit_range"))
    # bands = bkgpdf.createIntegral(var, RooFit.Range("low_range,high_range"))
    # print "bkgpdf eff range = ", full.getVal(), ", fit range = ", fit.getVal(), ", band range = ", bands.getVal(), "ratio = ", bands.getVal()/full.getVal()

  nsigeff = sigpdf.createIntegral(var, RooFit.Range("eff_range"))
  nbkgeff = bkgpdf.createIntegral(var, RooFit.Range("eff_range"))

  nsigfull = sigpdf.createIntegral(var, RooFit.Range("full_range"))

  nsigfit = sigpdf.createIntegral(var, RooFit.Range("fit_range"))
  nbkgfit = bkgpdf.createIntegral(var, RooFit.Range("fit_range"))

  sigeratio = nsigeff.getVal()/nsigfit.getVal()
  bkgeratio = nbkgeff.getVal()/nbkgfit.getVal()

  nsighist = sig.GetSumOfWeights()
  nbkghist = bkg.GetSumOfWeights()

  varnsig = RooRealVar("nsig", "nsig", 0.90*ndata_fit, 0.20*ndata_fit,     ndata_fit)
  varnbkg = RooRealVar("nbkg", "nbkg", 0.10*ndata_fit, 0.01*ndata_fit, 0.8*ndata_fit)

  nsig_pre = varnsig.getVal()

  pdf = RooAddPdf("pdf", "pdf", RooArgList(bkgpdf, sigpdf), RooArgList(varnbkg, varnsig))

  fit = pdf.fitTo(hdata, RooFit.SumW2Error(True), RooFit.Save(), RooFit.Range("fit_range"), RooFit.SumCoefRange("fit_range"), RooFit.PrintLevel(-1000))

  # print "Pre-signal hist (full):", nsighist
  # print "Pre-signal pdf (full):", nsigfull.getVal()
  # print "Pre-signal pdf (fit):", nsigfit.getVal()
  # # print "Pre-signal eff:", nsig_eff.getVal()
  # print "Pre-signal var (fit):", nsig_pre
  # print "Post-signal var (fit):", varnsig.getVal()
  # print "Post-bakground fit (full):", varnbkg.getVal()
  # print "nmc:", (varnsig.getVal()+varnbkg.getVal())
  # print "ndata:", ndata_fit

  if plotname != "":
    c1 = ROOT.TCanvas("c", "c", 600, 600)
    c1.SetLogy()

    axis = mass.frame(RooFit.Title("Template Fit"))

    hdata.plotOn(axis)
    pdf.plotOn(axis, RooFit.Components("bkgpdf"), RooFit.LineStyle(ROOT.kDashed), RooFit.LineColor(ROOT.kBlue), RooFit.Range("full_range"))
    pdf.plotOn(axis, RooFit.Components("sigpdf"), RooFit.LineColor(ROOT.kBlack), RooFit.Range("full_range"))
    pdf.plotOn(axis, RooFit.LineColor(ROOT.kRed), RooFit.Range("full_range"))
    hdata.plotOn(axis)

    axis.Draw()

    c1.SaveAs(plotname+".png")

  ndata_mcratio = ndata_eff * varnsig.getVal()*sigeratio/(varnsig.getVal()*sigeratio + varnbkg.getVal()*bkgeratio)
  ndata_sigfit = varnsig.getVal()*sigeratio

  fitsys = sqrt(1/varnsig.getVal() + 1/varnbkg.getVal())

  # print "Ndata MC ratio:",ndata_mcratio
  # print "Ndata MC fit var:",ndata_sigfit

  return ndata_mcratio, fitsys

def getSFs(mcgraph, dgraph, name, error = "none"):
  sf = dgraph.Clone()
  sf.SetName(name)
  sf.SetTitle(name)

  npoints = sf.GetN()
  for p in range(npoints):
    sf.SetPointY(p, dgraph.GetPointY(p)/mcgraph.GetPointY(p))

  if error == "none":
    for p in range(npoints):
      sf.SetPointEYlow(p, 0)
      sf.SetPointEYhigh(p, 0)
    return sf

  if error == "nadezda":
    for p in range(npoints):
      ed = hypot(dgraph.GetPointEYlow(p), dgraph.GetPointEYhigh(p))
      emc = hypot(mcgraph.GetPointEYlow(p), mcgraph.GetPointEYhigh(p))
      e = hypot(ed, emc)

      sf.SetPointEYlow(p, e)
      sf.SetPointEYhigh(p, e)
    return sf

  raise Exception('templatefit.py :: getSFs() :: unknown error type requested')

  return sf
