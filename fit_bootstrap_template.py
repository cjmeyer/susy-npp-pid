#!/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.151/InstallArea/x86_64-centos7-gcc8-opt/bin/python

import ROOT
import array
import templatefit
import boothelp

ROOT.gROOT.SetBatch(True)

# channels = ["ll", "ee", "mumu"]
channels = ["ll"]

# convType = [0, 1, 2]
convType = [2]

etaRanges = [ [0.0,0.6], [0.06,0.8], [0.8,1.37], [1.52,1.81], [1.81,2.01], [2.01,2.37]]
# etaRanges = [ [0.0,0.6] ]
etaBins = len(etaRanges)

pTboundaries = array.array('d', [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 100])
# pTboundaries = array.array('d', [10, 15, 20])
pTbins = len(pTboundaries)-1

ofile = ROOT.TFile.Open("efficiency.root", "RECREATE")

for chan in channels:

  path = "templates/"
  dfile = ROOT.TFile.Open(path+"data_"+chan+".root", "READ")
  gfile = ROOT.TFile.Open(path+"Sh_"+chan+"g.root", "READ")
  jfile = ROOT.TFile.Open(path+"Py_"+chan+"j.root", "READ")

  nrep = 0

  for c in convType:
    for e in range(etaBins):
      lgam  = []
      lgamL = []

      ldata  = []
      ldataL = []
      for p in range(pTbins):
        print ""
        print "******** Fitting chan = "+chan+", c = "+str(c)+", eta["+str(e)+"] = ["+str(etaRanges[e][0])+","+str(etaRanges[e][1])+"], pT["+str(p)+"] = ["+str(pTboundaries[p])+","+str(pTboundaries[p+1])+"] **********"
        print ""

        # Get histograms for template fit
        postfix = "_conv"+str(c)+"_eta"+str(e)+"_pt"+str(p)
        dboot = dfile.Get("mllg_data"+postfix)
        gboot = gfile.Get("mllg_gam"+postfix)
        jboot = jfile.Get("mllg_jet"+postfix)

        dbootL = dfile.Get("mllg_dataL"+postfix)
        gbootL = gfile.Get("mllg_gamL"+postfix)
        jbootL = jfile.Get("mllg_jetL"+postfix)

        if nrep != dboot.GetNReplica():
          nrep = dboot.GetNReplica()
          print "--> Updating nrep to", nrep

        # for b in range(1):
        for b in range(nrep):

          if p == 0:
            lgam.append([])
            lgamL.append([])

            ldata.append([])
            ldataL.append([])

            if b == 0:
              lgam.append([])
              lgamL.append([])

              ldata.append([])
              ldataL.append([])

          if pTboundaries[p] < 25: # Do fit if lower bound is less than this
            # Get denominator from template fit
            postfix = chan+"_conv"+str(c)+"_eta"+str(e)+"_pt"+str(p)
            if b == 0:
              ndata, sys = templatefit.fitData(dboot.GetNominal(), gboot.GetNominal(), bkg = jboot.GetNominal(), nlow = 80, nhigh = 100, plotname = "plots/fit_"+postfix)
              ldata[b].append(int(ndata))
            ndata, sys = templatefit.fitData(dboot.GetReplica(b), gboot.GetReplica(b), bkg = jboot.GetReplica(b), nlow = 80, nhigh = 100)
            ldata[b+1].append(int(ndata))

            # Get numerator from template fit
            if b == 0:
              ndataL, sys = templatefit.fitData(dbootL.GetNominal(), gbootL.GetNominal(), bkg = jbootL.GetNominal(), nlow = 80, nhigh = 100, plotname = "plots/fitL_"+postfix)
              ldataL[b].append(min(ndata,ndataL))
            ndataL, sys = templatefit.fitData(dbootL.GetReplica(b), gbootL.GetReplica(b), bkg = jbootL.GetReplica(b), nlow = 80, nhigh = 100)
            ldataL[b+1].append(min(ndata,ndataL))
          else:
            if b == 0:
              ndata, err  = templatefit.getIntegralAndError(dboot.GetNominal() , 80, 100)
              ndataL, err = templatefit.getIntegralAndError(dbootL.GetNominal(), 80, 100)
              ldata[b].append(ndata)
              ldataL[b].append(ndataL)
            ndata, err  = templatefit.getIntegralAndError(dboot.GetReplica(b) , 80, 100)
            ndataL, err = templatefit.getIntegralAndError(dbootL.GetReplica(b), 80, 100)
            ldata[b+1].append(ndata)
            ldataL[b+1].append(ndataL)

          # Get MC eff numerator and denominator from integral
          if b == 0:
            ngamL, ngam = templatefit.scaleIntegralToPoissonError(gbootL.GetNominal(), gboot.GetNominal(), nlow = 80, nhigh = 100)
            lgam[b].append(ngam)
            lgamL[b].append(ngamL)
          ngamL, ngam = templatefit.scaleIntegralToPoissonError(gbootL.GetReplica(b), gboot.GetReplica(b), nlow = 80, nhigh = 100)
          lgam[b+1].append(ngam)
          lgamL[b+1].append(ngamL)

      print ""
      print "******* Efficiency for c = "+str(c)+", eta = ["+str(etaRanges[e][0])+","+str(etaRanges[e][1])+"] *********"
      print ""
      # print "ldata:",ldata
      # print "ldataL:",ldataL
      # print "lgam:",lgam
      # print "lgamL:",lgamL

      # Make and save efficiency histograms
      postfix = "_conv"+str(c)+"_eta"+str(e)
      ofile.cd()

      mceff_reps = []
      deff_reps = []

      mceff_reps.append(templatefit.getEff(lgamL[0], lgam[0], pTboundaries, "mceff_"+chan+postfix))
      deff_reps.append(templatefit.getEff(ldataL[0], ldata[0], pTboundaries, "deff_"+chan+postfix))

      for b in range(1,nrep+1):
        mceff_reps.append(templatefit.getEff(lgamL[b], lgam[b], pTboundaries, "mceff_"+chan+postfix+"_rep"+str(b)))
        deff_reps.append(templatefit.getEff(ldataL[b], ldata[b], pTboundaries, "deff_"+chan+postfix+"_rep"+str(b)))

      # print "mceff_reps:",mceff_reps[0].Print("all")
      # print "deff_reps:",deff_reps[0].Print("all")

      # mceff = boothelp.getGraphMeanRMS(mceff_reps, "mceff_"+chan+postfix)
      # print "mceff:",mceff.Print("all")
      mceff_reps[0].Write()

      # deff = boothelp.getGraphMeanRMS(deff_reps, "deff_"+chan+postfix)
      # print "deff:",deff.Print("all")
      deff_reps[0].Write()

      sf_reps = []
      sf_reps.append(templatefit.getSFs(mceff_reps[0], deff_reps[0], "sf_"+chan+postfix, error = ""))
      for b in range(1,nrep+1):
        sf_reps.append(templatefit.getSFs(mceff_reps[b], deff_reps[b], "sf_"+chan+postfix+"_rep"+str(b)))
      
      sf_reps[0].Write()
      sf = boothelp.getGraphMeanRMS(sf_reps[1:], "sf_"+chan+postfix+"_bootstrap")
      sf.Write()

      # print "mc loose:",lgamL
      # print "mc all:",lgam

      # print "data loose:",ldataL
      # print "data all:",ldata

      # eff = templatefit.getEffNadezda(lgamL, lgam, pTboundaries, "mceff_"+chan+postfix)
      # mceff = templatefit.getEff(lgamL, lgam, pTboundaries, "mceff_"+chan+postfix)
      # mceff.Write()

      # eff = templatefit.getEffNadezda(ldataL, ldata, pTboundaries, "deff_"+chan+postfix)
      # deff = templatefit.getEff(ldataL, ldata, pTboundaries, "deff_"+chan+postfix)
      # deff.Write()

      # sf = templatefit.getSFs(mceff, deff, "sf_"+chan+postfix)

