import ROOT

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.SetStyle("ATLAS")

ifile = ROOT.TFile.Open("efficiency.root")

mc = ifile.Get("mceff_ll_conv2_eta5")
data = ifile.Get("deff_ll_conv2_eta5")

data.SetMarkerColor(ROOT.kBlack)
data.SetLineColor(ROOT.kBlack)
data.SetMarkerStyle(8)

mc.SetLineColor(ROOT.kRed)
mc.SetMarkerColor(ROOT.kRed)
mc.SetMarkerStyle(4)

c = ROOT.TCanvas("c","c",800,600)

data.GetXaxis().SetTitle("p_{T} [GeV]")
data.GetYaxis().SetTitle("Efficiency")
data.GetYaxis().SetRangeUser(0.7, 1.2)
data.Draw("ape")

mc.Draw("pe")

c.SaveAs("plots/efficiency.png")
