mkdir -p templates
cp /P/bolt/data/cjmey/condor/out/make_templates/*.root templates/

channels="
ee
mumu
"

for chan in $channels; do

  hadd -f templates/data_${chan}.root templates/data_*_*${chan}g.root
  
  hadd -f templates/Sh_${chan}g.root templates/mc16*_Sh_${chan}g.root
  # hadd -f templates/Sh_${chan}j.root templates/mc16*_Sh_${chan}j.root
  
  hadd -f templates/Py_${chan}g.root templates/mc16*_Py_${chan}.root
  hadd -f templates/Py_${chan}j.root templates/mc16*_Py_${chan}.root

done

# Both e and mu
hadd -f templates/data_ll.root templates/data_1*_*g.root

hadd -f templates/Sh_llg.root templates/mc16*_Sh_*g.root
# hadd -f templates/Sh_llj.root templates/mc16*_Sh_*j.root

hadd -f templates/Py_llg.root templates/mc16*_Py_*.root
hadd -f templates/Py_llj.root templates/mc16*_Py_*.root

