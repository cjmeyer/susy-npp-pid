#!/usr/bin/env python
import os
import condor

pwd = os.environ["PWD"]
nfs_area = "/P/bolt/data/" + os.environ["USER"] + "/condor"

# File to execute (do not include .py) 
execute_file = "make_templates_orig" #no spaces between ""

# Configuration common to all jobs
basejob = condor.JobSpec()
basejob.job_inputs = [pwd +"/" + execute_file+ ".py"]
basejob.job_setup = pwd + "/setup_node.sh"
basejob.output_dir = nfs_area + "/out/"+execute_file

helper = condor.CondorSubmit(cleanOldJobs=True, inputSamples="local")

# Samples to run over
ntup_path = "/P/bolt/scratch/cjmey/data/NTUP_ZLLG-2020-07-22"
samples = {
  "mc16a" : {
    "Sh_eeg"   : "mc16a_13TeV/Sh_eeg",
    "Sh_mumug" : "mc16a_13TeV/Sh_mumug",
    "Py_ee"    : "mc16a_13TeV/Py_Zee",
    "Py_mumu"  : "mc16a_13TeV/Py_Zmumu",
  },
  "mc16d" : {
    "Sh_eeg"   : "mc16d_13TeV/Sh_eeg",
    "Sh_mumug" : "mc16d_13TeV/Sh_mumug",
    "Py_ee"    : "mc16d_13TeV/Py_Zee",
    "Py_mumu"  : "mc16d_13TeV/Py_Zmumu",
  },
  "mc16e" : {
    "Sh_eeg"   : "mc16e_13TeV/Sh_eeg",
    "Sh_mumug" : "mc16e_13TeV/Sh_mumug",
    "Py_ee"    : "mc16e_13TeV/Py_Zee",
    "Py_mumu"  : "mc16e_13TeV/Py_Zmumu",
  },
  "data" : {
    "15_eeg" : "data15_13TeV/target_data15_Zeeg_p3948.root",
    "16_eeg" : "data16_13TeV/target_data16_Zeeg_p3948.root",
    "17_eeg" : "data17_13TeV/target_data17_Zeeg_p3948.root",
    "18_eeg" : "data18_13TeV/target_data18_Zeeg_p3948.root",

    "15_mumug" : "data15_13TeV/target_data15_Zmumug_p3948.root",
    "16_mumug" : "data16_13TeV/target_data16_Zmumug_p3948.root",
    "17_mumug" : "data17_13TeV/target_data17_Zmumug_p3948.root",
    "18_mumug" : "data18_13TeV/target_data18_Zmumug_p3948.root",
  }
}

for dtype in samples:
  for sample in samples[dtype]:
    job = basejob

    name = dtype + "_" + sample
    job.name = name
    job.job_area = nfs_area + "/log/" + name

    job.input_dir = ntup_path + "/" + samples[dtype][sample]
    job.run_command = "python "+ execute_file+".py " + name

    helper.submitJob(job)
