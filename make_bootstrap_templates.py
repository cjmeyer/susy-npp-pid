import sys
import logging
import ROOT
import array

# Initialize
APP_Name = sys.argv[0]
max_events = -1

logging.basicConfig(level=logging.DEBUG, format='%(name)-25s %(levelname)-7s %(message)s')
msg = logging.getLogger(APP_Name)

if len(sys.argv) < 2:
  msg.error("Need to pass at least one argument")
  sys.exit(1)
data_type = sys.argv[1]

# Get input file
input_list = open("input_data.txt", "rU")
sample = ""

for line in input_list:
  sample  = line.split("\n")[0]
input_list.close()

ifile = ROOT.TFile.Open(sample)
ofile = ROOT.TFile.Open("output.root", "RECREATE")

if not ifile:
  msg.error("Couldn't open input file")
  sys.exit(1)

t = ifile.Get("output")

# Setup histograms

etaRanges = [ [0.0,0.6], [0.6,0.8], [0.8,1.37], [1.52,1.81], [1.81,2.01], [2.01,2.37]]
etaBins = len(etaRanges)

pTboundaries = array.array('d', [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 100])
pTbins = len(pTboundaries)-1

cBins = 3

m_llg_data  = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]
m_llg_dataL = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]
m_llg_gam   = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]
m_llg_gamL  = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]
m_llg_jet   = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]
m_llg_jetL  = [[[ None for p in range(pTbins) ] for e in range(etaBins) ] for c in range(cBins) ]

n_m_llg    = 40
m_llg_low  = 65
m_llg_high = 105
nrep       = 100

for c in range(cBins):
  for e in range(etaBins):
    for p in range(pTbins):
      postfix = "_conv"+str(c)+"_eta"+str(e)+"_pt"+str(p)

      m_llg_data[c][e][p]  = ROOT.TH1DBootstrap("mllg_data" +postfix, "mllg_data" +postfix, n_m_llg, m_llg_low, m_llg_high, nrep) # Nadezda did 50, 65, 105
      m_llg_dataL[c][e][p] = ROOT.TH1DBootstrap("mllg_dataL"+postfix, "mllg_dataL"+postfix, n_m_llg, m_llg_low, m_llg_high, nrep)

      m_llg_gam[c][e][p]   = ROOT.TH1DBootstrap("mllg_gam"  +postfix, "mllg_gam"  +postfix, n_m_llg, m_llg_low, m_llg_high, nrep)
      m_llg_gamL[c][e][p]  = ROOT.TH1DBootstrap("mllg_gamL" +postfix, "mllg_gamL" +postfix, n_m_llg, m_llg_low, m_llg_high, nrep)

      m_llg_jet[c][e][p]   = ROOT.TH1DBootstrap("mllg_jet"  +postfix, "mllg_jet"  +postfix, n_m_llg, m_llg_low, m_llg_high, nrep)
      m_llg_jetL[c][e][p]  = ROOT.TH1DBootstrap("mllg_jetL" +postfix, "mllg_jetL" +postfix, n_m_llg, m_llg_low, m_llg_high, nrep)

pt_all = ROOT.TH1D("pt_all", "pt_all", 50, 0, 500)

# Loops over input file
entry = 0
for ev in t:
  if (max_events > -1 and entry > max_events):
    msg.info("Exiting after manually specified "+str(max_events)+" events")
    break

  if (entry%1000 == 0):
    msg.info("Running over event "+str(entry)+" out of "+str(t.GetEntries()) )

  entry = entry + 1

  # Connect to variables in tree
  m_llg    = getattr(ev, "llg.m")/ 1000.0
  m_ll     = getattr(ev, "ll.m")/1000.0
  pt       = getattr(ev, "ph.pt")/1000.0
  pt2      = getattr(ev, "ph.pt2")/1000.0
  isLoose  = getattr(ev, "ph.loose_id")
  eta	   = abs(getattr(ev, "ph.eta2"))
  dRmin    = getattr(ev, "ph_l.dRmin")	
  isIso    = getattr(ev, "ph.isoloose")	
  convFlag = getattr(ev, "ph.convFlag")	
  runNum   = getattr(ev, "EventInfo.runNumber")
  evNum    = getattr(ev, "EventInfo.eventNumber")
  mcID     = 0

  etcone20 = getattr(ev, "ph.topoetcone20")/1000.0
  ptcone20 = getattr(ev, "ph.ptcone20_TightTTVA_pt1000")/1000.0

  ph_origin = ph_type = 0

  if "mc" in data_type:
    w_pu      = getattr(ev, "mc_weight.pu")
    w_gen     = getattr(ev, "mc_weight.gen")
    w_xs      = getattr(ev, "mc_weight.xs")
    w_lumi    = getattr(ev, "mc_weight.lumi")
    w_l1      = getattr(ev, "mc_weight.l1")
    w_l2      = getattr(ev, "mc_weight.l2")
    w_trig    = getattr(ev, "mc_weight.trigger")
    w_iso     = getattr(ev, "mc_weight.phISO_FixedCutLoose")
    ph_origin = getattr(ev, "ph.truth_origin")
    ph_type   = getattr(ev, "ph.truth_type")	
    ph_pdgId  = getattr(ev, "ph.truth_pdgId")
    mcID      = getattr(ev, "mc.channelNumber")

  # Event weight for MC
  w = 1.0
  if "mc" in data_type:
    w *= w_pu*w_gen*w_xs*w_l1*w_l2*w_trig*w_iso*w_lumi
  # if "mc16a" in data_type:
  #   w *= 36.1
  # if "mc16d" in data_type:
  #   w *= 44.3
  # if "mc16e" in data_type:
  #   w *= 59.1

  pt_all.Fill(pt, w)

  # # NADEZDA does this
  # if eta > 2.37:
  #   continue

  # # NADEZDA does this
  # if pt2 < 10 or pt2 > 100:
  #   continue

  # Apply event selection
  if "ee" in data_type and dRmin < 0.4:
    continue

  # if "mumu" in data_type and dRmin < 0.2:
  #   continue

  # NADEZDA does this
  if (etcone20 > 0.065*pt) or (ptcone20/pt > 0.05):
    continue

  # # NADEZDA doesn't do this
  # if not isIso:
  #   continue

  if m_ll > 83.0:
    continue

  # Fill histograms
  c = 0
  if convFlag > 0: c = 1

  for e in range(etaBins):
    if eta < etaRanges[e][0] or etaRanges[e][1] <= eta:
      continue

    for p in range(pTbins):
      if pt <= pTboundaries[p] or pTboundaries[p+1] <= pt:
        continue
      
      # print "c:",c,"eta:",eta,"pt:",pt,"m_llg:",m_llg,"m_ll:",m_ll,"dRmin:",dRmin,"isIso:",isIso,"w:",w

      m_llg_data[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
      if cBins > 2: m_llg_data[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)

      if isLoose:
        m_llg_dataL[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
        if cBins > 2: m_llg_dataL[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)

      if ("Py" in data_type and ph_origin == 40) or ("Sh" in data_type and ph_type == 13):
        m_llg_gam[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
        if cBins > 2: m_llg_gam[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)

        if isLoose:
          m_llg_gamL[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
          if cBins > 2: m_llg_gamL[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)

      if ("Py" in data_type and ph_origin != 40) or ("Sh" in data_type and ph_origin != 40):
        m_llg_jet[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
        if cBins > 2: m_llg_jet[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)

        if isLoose:
          m_llg_jetL[c][e][p].Fill(m_llg, w, runNum, evNum, mcID)
          if cBins > 2: m_llg_jetL[2][e][p].Fill(m_llg, w, runNum, evNum, mcID)


ofile.cd()

m_llg_data[0][0][0].GetNominal().Print("all")

for c in range(cBins):
  for e in range(etaBins):
    for p in range(pTbins):
      m_llg_data[c][e][p].Write()
      m_llg_dataL[c][e][p].Write()

      m_llg_gam[c][e][p].Write()
      m_llg_gamL[c][e][p].Write()

      m_llg_jet[c][e][p].Write()
      m_llg_jetL[c][e][p].Write()

pt_all.Write()
