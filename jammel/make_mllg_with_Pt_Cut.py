#The purpose of this code is to see how Pt (transverse momentum) changes in different bins of Eta

# Initial Libraries 
import sys, logging
import ROOT
import numpy
from array import array

# Setting up variables
APP_Name        =       "PID for loose Photons"
max_events      =  -1

# Setting up message logging
logging.basicConfig(level=logging.DEBUG, format='%(name)-25s %(levelname)-7s %(message)s')
msg             =       logging.getLogger(APP_Name)

if len(sys.argv) < 2:
	msg.error("Need to pass at least one argument")
	sys.exit(1)
data_type = sys.argv[1]
print "data_type ==", data_type
# Reading in File and Creating Output file
input_list      = open("input_data.txt", "rU")
sample          = ""

print "This is the samples name", sample
for line in input_list:
	sample  = line.split("\n")[0]
input_list.close()

msg.info("Trying to open: \"" +sample+"\"")

ifile           = ROOT.TFile.Open(sample)
ofile           = ROOT.TFile.Open("output.root", "RECREATE")

if not ifile:
	msg.error("Couldn't open input file")
	sys.exit(1)
t               = ifile.Get("output")


# Setting up ll mass for small max and min cut selections
ll_max          =       83
ll_min          =       40

# Setting up weight variables (wg = weight)
pu_wg           =       0
gen_wg          =       0
xs_wg           =       0
L1_wg           =       0
L1_wg           =       0
trigger_wg      =       0
ph_ISO_fixed    =       0

# Setting up llg and ll mass for larger max and min cut selections
llg_max2	=	105
llg_min2	=	65       

ll_max2 	=       105
ll_min2		=       65
 
in_wg		= 	0 #initial weight
scld_wg 	= 	0 # scaled weight 

#Setting up eta cut selections
etaBins     =   [ [0.0 , 0.6],[0.6,0.8], [0.8, 1.37], [1.52, 1.81], [1.81, 2.01], [2.01, 2.37], [0.0, 2.37]]

#Setting up Pt ranges
pTbins      =   [ [10.0,15.0],[15.0,20.0],[20.0,25.0],[25.0,30.0],[30.0,35.0],[35.0,40.0],[40.0,45.0], [45.0, 50.0], [50.0,60.0],[60.0,80.0],[80.0,100.0]]

#Setting up the naming schemes for the plots
convFlag	= [ "Combined_Conv_"]
#convFlag	= [ "Unconverted_", "Converted_", "Combined_Conv_"]

#Defining and creating arrays to store histograms for all photons and photons that pass the loose criteria
hist_mllg 	   			= []
hist_mllg_loose				= []

conV_llm				= []
conV_llgm				= []

hist_mllg_jet 	        		= []
hist_mllg_jet_loose    			= []

conV_jet			    	= []

hist_mllg_gam 	  	    		= []
hist_mllg_gam_loose	    		= []

conV_pt					= []
conV_pt_loose				= []

conv_Z					= []

pu_wg_histo       			= []
gen_wg_histo       			= []
xs_wg_histo        			= []

for c in range( len(convFlag) ): #Converted or Unconverted
	conV_llm.append([])
	conV_llgm.append([])

	conV_jet.append([])
	conV_pt.append([])
	conV_pt_loose.append([])
	
	hist_mllg.append( []) 
	hist_mllg_loose.append( []) 
		
	hist_mllg_jet.append([])
	hist_mllg_jet_loose.append([])

	hist_mllg_gam.append([])
	hist_mllg_gam_loose.append([])
	
	conv_Z.append([])

	pu_wg_histo.append(ROOT.TH1F(convFlag[c]+"pu_wg", convFlag[c]+"pu_wg", 110, 0.0, 5.0))
	gen_wg_histo.append(ROOT.TH1F(convFlag[c]+"gen_wg", convFlag[c]+"gen_wg", 110, -5.0, 5.0))
	xs_wg_histo.append(ROOT.TH1F(convFlag[c]+"xs_wg", convFlag[c]+"xs_wg", 110, 0.0, 50.0))

	for e in range(len( etaBins )):#Number of eta bins
		conV_llm[c].append(ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"llm", convFlag[c]+"Eta Bin_"+str(e)+"Mass of ll (mass range 105-65 GeV)", 110, 50.0, 100.0))
		conV_llgm[c].append(ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"llgm", convFlag[c]+"Eta Bin_"+str(e)+"Mass of llg (mass range 105-65 GeV)", 110, 50.0, 100.0))

		conV_pt[c].append(ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"pt_all", convFlag[c]+"Eta Bin_"+str(e)+"pt",40, 0.0, 200.0))
		conV_pt_loose[c].append(ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"pt_loose", convFlag[c]+"Eta Bin_"+str(e)+"_pt_loose",40, 0.0, 200.0))

		conV_jet[c].append(ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"_jet_ph",convFlag[c]+"Eta Bin_"+str(e)+"jet_ph", 60, 0.0, 50.0))
	
		conv_Z[c].append(   ROOT.TH1F(convFlag[c]+"_Eta"+str(e)+"Z",convFlag[c]+"Eta Bin_"+str(e)+"Z", 100, -250.0, 250.0)   )

		hist_mllg[c].append([])
		hist_mllg_loose[c].append([])
		
		hist_mllg_jet[c].append([])
		hist_mllg_jet_loose[c].append([])

		hist_mllg_gam[c].append([])
		hist_mllg_gam_loose[c].append([])

		for pT in range( len(pTbins) ): # Number of pT bins
			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_"+str(pT),convFlag[c]+"_mllg - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg[c][e].append(hist)

			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_loose_"+str(pT),convFlag[c]+"_mllg_loose - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg_loose[c][e].append(hist)
				
			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_jet_"+str(pT),convFlag[c]+"_mllg_jet - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg_jet[c][e].append(hist)


			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_jet_loose_"+str(pT),convFlag[c]+"_mllg_jet_loose - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg_jet_loose[c][e].append(hist)


			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_gam_"+str(pT),convFlag[c]+"_mllg_gam - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg_gam[c][e].append(hist)


			hist    = ROOT.TH1F(convFlag[c]+"Eta_"+str(e)+"_mllg_gam_loose_"+str(pT),convFlag[c]+"_mllg_gam_loose - Eta Cut_"+str(e)+"_pT Bin #"+str(pT), 16, 65, 105)
			hist_mllg_gam_loose[c][e].append(hist)
				

# Setting up loop increment
entry = 0
# Loops over input file
for ev in t:
	
	#Output message about events running over and when it's exiting after # of events	
	if (max_events > -1 and entry > max_events):
		msg.info("Exiting after manually specified "+str(max_events)+" events")
		break

	if	(entry%1000 == 0):
		msg.info("Running over event "+str(entry)+" out of "+str(t.GetEntries()) )
	
	# loop increment
	entry	= entry +1
		
	# Getting variables out of saved data TTree
	llg_m	= getattr(ev, "llg.m")/ 1000.0
	ll_m	= getattr(ev, "ll.m")/1000.0
	pt		= getattr(ev, "ph.pt")/1000.0
	pt_l	= getattr(ev, "ph.loose_id")
	eta		= abs(getattr(ev, "ph.eta2"))
	ph_l	= getattr(ev, "ph_l.dRmin")	
	phIso   = getattr(ev, "ph.isoloose")	
	convflag = getattr(ev, "ph.convFlag")	
	primaryZ = getattr(ev, "PrimaryVertex.z")

	ph_truth_origin = 40
	ph_truth_type=0	

	if "mc" in data_type:
		# Getting weight variables (wg = weight) out of saved data TTree
		pu_wg           =       getattr(ev, "mc_weight.pu")
		gen_wg          =       getattr(ev, "mc_weight.gen")
		xs_wg	        =       getattr(ev, "mc_weight.xs")
		L1_wg           =       getattr(ev, "mc_weight.l1")
		L2_wg           =       getattr(ev, "mc_weight.l2")
		trigger_wg      =       getattr(ev, "mc_weight.trigger")
		ph_ISO_fixed    =       getattr(ev, "mc_weight.phISO_FixedCutLoose")
		ph_truth_origin =		getattr(ev, "ph.truth_origin")
		ph_truth_type   =		getattr(ev, "ph.truth_type")	
		
		#Filling histograms to look at the weights
		pu_wg_histo[c].Fill(pu_wg)
		if "jet" in data_type:
			if gen_wg >2: 
				gen_wg =2
			elif gen_wg<-2:
				gen_wg= -2
		gen_wg_histo[c].Fill(gen_wg)
		xs_wg_histo[c].Fill(xs_wg)
	

	# Setting up weight variables (wg = weight)
	if "mc" in data_type:
		in_wg = pu_wg*gen_wg*xs_wg*L1_wg*L2_wg*trigger_wg*ph_ISO_fixed
	#	wg = pu_wg*gen_wg*xs_wg
	if "mc16a" in data_type:
		scld_wg = in_wg*36.1

	if "mc16d" in data_type:
		scld_wg = in_wg*44.3

	if "mc16e" in data_type:
		scld_wg = in_wg*59.1

	# Ensuring that the mass of llg or ll are within the range of adding up to be the mass of a Z boson (Second Large Mass Selection)
	if (ll_m > 83.0):
		continue

	if scld_wg == 0:
		scld_wg=1

	if data_type.find("ee") != -1:
		# this is an electron sample
		if ph_l < 0.2:
			continue

	if data_type.find("mumu") != -1:
		# this is an mumu sample		
		if ph_l < 0.4:
			continue


	if not phIso: 
		continue 

	# Ensuring that the Mass of llg or ll are outside the range of ading up to be the mass of a Z boson (First smaller Mass selection)
	#llg_min2 == 65
	#llg_max2 == 105
	if (llg_min2 > llg_m or llg_m > llg_max2):
		continue

	if convflag ==0 or convflag ==1: 
		c=0 #Combined conversions


	# Making the Cut Selections Based on the Eta Cut Selection Range
	for e in range(len( etaBins )): #Number of eta bins

		# Making eta cut selections
		if etaBins[e][0] < eta and eta < etaBins[e][1]:
			conV_pt[c][e].Fill(pt,scld_wg)
			conV_llgm[c][e].Fill(llg_m,scld_wg)
			conV_llm[c][e].Fill(ll_m,scld_wg)
			conV_jet[c][e].Fill(ph_truth_origin)
			conv_Z[c][e].Fill(primaryZ)
			if pt_l: #Loose
				conV_pt_loose[c][e].Fill(pt,scld_wg)

			#Making pt cut selection	
			for pT in range( len(pTbins) ): #Number of pT bins
				if pt >= pTbins[pT][0] and pt <= pTbins[pT][1]:
					hist_mllg[c][e][pT].Fill(llg_m,scld_wg)
					if pt_l: #loose
						hist_mllg_loose[c][e][pT].Fill(llg_m,scld_wg)
					if "Sh" in sample:
						if (ph_truth_type != 40): #Sherpa
							hist_mllg_jet[c][e][pT].Fill(llg_m,scld_wg)							
						if (ph_truth_type == 13):#Sherpa
							hist_mllg_gam[c][e][pT].Fill(llg_m,scld_wg)

						if pt_l:
							if (ph_truth_type != 40): #Sherpa
								hist_mllg_jet_loose[c][e][pT].Fill(llg_m,scld_wg)
							if (ph_truth_type == 13): #Sherpa
								hist_mllg_gam_loose[c][e][pT].Fill(llg_m,scld_wg)
					
					elif "Py" in sample:
						if (ph_truth_origin != 40): #powheg
							hist_mllg_jet[c][e][pT].Fill(llg_m,scld_wg)							
						if (ph_truth_origin == 40): #powheg
							hist_mllg_gam[c][e][pT].Fill(llg_m,scld_wg)
	
						if pt_l:#loose
							if (ph_truth_origin != 40 ): #powheg
								hist_mllg_jet_loose[c][e][pT].Fill(llg_m,scld_wg)
							if (ph_truth_origin == 40): #powheg
								hist_mllg_gam_loose[c][e][pT].Fill(llg_m,scld_wg)
	

# Writing Histograms
for c in range( len(convFlag) ): #Converted or Unconverted
	
	pu_wg_histo[c].Write()
	gen_wg_histo[c].Write()
	xs_wg_histo[c].Write()
	
	for e in range(len( etaBins )):#Number of eta bins
		
		conV_llm[c][e].Write()

		conV_llgm[c][e].Write()

		conV_pt[c][e].Write()

		conV_pt_loose[c][e].Write()

		conV_jet[c][e].Write()

		conv_Z[c][e].Write()
			
		for pT in range( len(pTbins)): # Number of pT bins
			hist_mllg[c][e][pT].Write()
			hist_mllg_loose[c][e][pT].Write()
				
			hist_mllg_jet[c][e][pT].Write()
			hist_mllg_jet_loose[c][e][pT].Write()

			hist_mllg_gam[c][e][pT].Write()
			hist_mllg_gam_loose[c][e][pT].Write()
				



ofile.Close()












