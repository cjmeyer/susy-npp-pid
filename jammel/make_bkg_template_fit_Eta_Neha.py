# Importing Libraries
import ROOT
from array import array
import numpy as np
import sys, logging
import os
from ROOT import RooFit, RooRealVar,RooFitResult, RooArgList, RooHistPdf, RooArgSet, RooDataHist,RooPlot, RooAddPdf, RooPolynomial ,TCanvas, RooGaussian, RooDataSet, RooAbsData, RooCmdArg, TCanvas, TAxis, TLine, TLatex, TMarker, TPave, TString, RooPlot, TAttLine
from math import *

#Set ATLAS style for plots
ROOT.gROOT.SetStyle("ATLAS")

# Prevents popus/graphis from appearing on screen
ROOT.gROOT.SetBatch(True)

#Creating output file
ofile           = ROOT.TFile.Open("bkg_templateSherp_Pow.root", "RECREATE")
#ofile           = ROOT.TFile.Open("bkg_templateReta_Rhad.root", "RECREATE")

#Loop switches between Converted and Unconverted Photons
convFlag =["Combined_Conv_"]

#Loop runs over Eta cut selection 
cutSelection =["Eta_"]
d=["*data*.root"]
m=["*mc*Sh*.root", "*mc*Py*.root"]
plot_name= ["Sherpa_Zll", "Powheg_Zll"]
dataFlag=["Sh_Combined_Zll","Py_Combined_Zll"]


#Other options to run over
#m=["*mc*Py_Z*.root"]
#dataFlag=["Py_Combined_Zll"]
#m=["*mc*Py*reta*.root", "*mc*Py*rhad*.root"]
#dataFlag=["Py_Reta_Zll", "Py_Rhad_Zll"]
#convFlag =["Unconverted_","Converted_","Combined_Conv_"]
#dataFlag=["Py_Combined_Zll"]
#m=["*mc*Py*.root"]


#Creating a name for the loop increment
#plot_name = ["Py_Reta_Zll", "Py_Rhad_Zll"]
#plot_name= ["Powheg_Zll"]

bkg_m=["#it{ll}"]

#Directory location where data is stored
nfs_area = "/P/bolt/data/" + os.environ["USER"]+"/d05/" + "condor"

execute_file = "make_mllg_with_Pt_Cut"
path = nfs_area + "/out/"+execute_file

#Directory location where plots are stored
pwd = os.environ["PWD"]
executable_dir_path = pwd+"/plots" +"/"+execute_file

#Defining variables
x=ROOT.Double( 0.0)
y_mc=ROOT.Double( 0.0)
y_data=ROOT.Double(0.0)
y_line=ROOT.Double(0.0)


#Setting up Eta ranges
etaBins     =   [ [0.0 , 0.6],[0.6,0.8], [0.8, 1.37], [1.52, 1.81], [1.81, 2.01], [2.01, 2.37]]
netaBins   	=	len(etaBins)

#Bins that were used for initial poly hist fit
#pTbins		=	[10.0,15.0,20.0,25.0,30.0,35.0,40.0,45.0,50.0,60.0,100.0]
#Bins that Nadezda uses
pTbins		=	[10.0,15.0,20.0,25.0,30.0,35.0,40.0,45.0,50.0,60.0,80.0,100.0]
nptBins		=	len(pTbins)-1	

for conV in range(len(convFlag)):
	#Loop switches Sherpa and Powheg
	for Z in range(len(dataFlag)):

		#Merging Data 2015-2018
		print "Running over "+dataFlag[Z]+"g data years 2015 - 2018"
		os.chdir(path)
		data_file = path+"/data18_15_"+dataFlag[Z]+"g.root"

		if os.path.exists(data_file):
			print "Combined Data file for data 2015-2018 for "+dataFlag[Z]+"g already exists"
			print "data18_15_"+dataFlag[Z]+"g.root"
		else: 
			os.system("hadd -f data18_15_"+dataFlag[Z]+"g.root "+ d[0])
			print "data18_15_"+dataFlag[Z]+"g.root"
			print "Created combined Data File for data15 through data18 for "+dataFlag[Z]+"g"
			
		data_file = path+"/mc16a_e_"+dataFlag[Z]+"_Pt.root"

		#Merging signal MC mc16(a,d,e) for Powheg or sherpa
		if os.path.exists(data_file):
			print "Combined Data file for mc16a, mc16d and mc16e "+dataFlag[Z]+"g already exists"
			print "mc16a_e_"+dataFlag[Z]+"_Pt.root"
		else: 
			os.system("hadd -f mc16a_e_"+dataFlag[Z]+"_Pt.root "+ m[Z])
			print "hadd -f mc16a_e_"+dataFlag[Z]+"_Pt.root "+ m[Z]
			print "Created combined Data File for mc16a, mc16d and mc16e for gammaPt"


		data_file = path+"/mc16a_e_bkg_jet_Pt.root"
		#Mergine background MC
		if os.path.exists(data_file):
			print "Combined Data file mc16a, mc16d and mc16e bkg_jet for already exists"
		else: 
			os.system("hadd -f mc16a_e_bkg_jet_Pt.root mc1*_Py*Pt*root")
			print "mc16a_e_bkg_jet_Pt.root"
			print "Created combined Data File for mc16a, mc16d and mc16e for jet"


		# Opening and Reading Data Files
		inFile1 = ROOT.TFile.Open("data18_15_"+dataFlag[Z]+"g.root","READ")
		inFile2 = ROOT.TFile.Open("mc16a_e_bkg_jet_Pt.root" ,"READ") #Use same bkg for Powheg and Sherpa therefore it stays constant
		inFile3 = ROOT.TFile.Open("mc16a_e_"+dataFlag[Z]+"_Pt.root" ,"READ")
			
		for e  in range(netaBins):
			myEff_d_Zll = ROOT.TH1F(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"d_Zll_All_Photons",plot_name[Z]+"Zll_DATA_All_Photons"+ str(e), nptBins,array('d', pTbins))	
			myEff_d_Zll_loose = ROOT.TH1F(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"d_Zll_loose_photons",plot_name[Z]+"Zll_DATA_Loose_Photons"+ str(e), nptBins,array('d', pTbins))	
					
			myEff_mc_Zll = ROOT.TH1F (convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"mc_Zll_All_photons", plot_name[Z]+"Zll_MC_All_photons"+ str(e),nptBins,array('d', pTbins))	
			myEff_mc_Zll_loose = ROOT.TH1F (convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"mc_Zll_loose_photons", plot_name[Z]+"Zll_MC_loose_photons"+ str(e),nptBins,array('d', pTbins))	
				
			myEff_d_Zll_corr = ROOT.TH1F(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"d_Zll_corr_All_Photons", plot_name[Z]+"Zll_Data_Corrected_All_Photons",nptBins,array('d', pTbins))
			myEff_d_Zll_corr_loose = ROOT.TH1F(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"d_Zll_corr_loose_photons", plot_name[Z]+"Zll_Data_Corrected_loose_photons",nptBins,array('d', pTbins))


				
			print "nptBins", nptBins
			for nptB in range(nptBins):
				print "Loop number for nptBins", nptB
				# All photons
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_"+str(nptB)

				Mllg_data15_17 = hist_name
				
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_jet_"+str(nptB)
				Mllg_Zee_jet = hist_name
					
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_gam_"+str(nptB)
				Mllg_eegammaPt = hist_name

				#Photons that fit Loose Criteria
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_loose_"+str(nptB)
				Mllg_data15_17_loose = hist_name
					
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_jet_loose_"+str(nptB)
				Mllg_Zee_jet_loose = hist_name
			
				hist_name =convFlag[conV]+"Eta_"+str(e)+"_mllg_gam_loose_"+str(nptB)
				Mllg_eegammaPt_loose = hist_name

				#setting bkgTemplate 
				bkgTemplate = 0

				#Checking to see if the histograms exist

				#All Photons
				histSignal1= inFile3.Get(Mllg_eegammaPt)
				if not histSignal1:
					print "Failed to get histogram = ", Mllg_eegammaPt
					print "Signal1 Eta bin number: "+str(e)+" Pt Bin number: "+str(nptB)
					sys.exit(1)
			
				histBkg1= inFile2.Get(Mllg_Zee_jet)
				if not histBkg1:
					print "Failed to get histogram = ", Mllg_Zee_jet
					print "Eta bin number: "+str(e)+" Pt Bin number: "+str(nptB)
					sys.exit(1)	
				if  histBkg1.GetEntries() == 0: 
					histBkg1.Fill(91, 0.001)

				histData1=inFile1.Get(Mllg_data15_17)
				if not histData1:
					print "Failed to get histogram = ", Mllg_data15_17
					print "Data1 Eta bin number: "+str(e)+" Pt Bin number: "+str(nptB)
					sys.exit(1)	

				#Photons with Loose Criteria
				histSignal2= inFile3.Get(Mllg_eegammaPt_loose)
				if not histSignal2:
					print "Failed to get histogram = ", Mllg_eegammaPt_loose
					print "Signal2 Eta bin number: "+str(e)+" Pt Bin number: "+str(nptB)
					sys.exit(1)	

				histBkg2= inFile2.Get(Mllg_Zee_jet_loose)
				if not histBkg2:
					print "Failed to get histogram = ", Mllg_Zee_jet_loose
					print "Eta bin number: "+str(e)+" Pt Bin number: "+str(nptB)
					sys.exit(1)	
				if histBkg2.GetEntries() == 0:
					histBkg2.Fill(91, 0.001)

				histData2=inFile1.Get(Mllg_data15_17_loose)
				if not histData2:
					print "Failed to get histogram = ", Mllg_data15_17_loose
					print "Data2 Eta bin number: "+str(e)+"| Pt Bin number: "+str(nptB)
					sys.exit(1)	

				
				# Defining Variables
				luminosity	= 139.0

				eff_nocorr_central=0.0
				eff_nocorr_syst=0.0
				eff_mc_central=0.0
				eff_mc_syst = 0.0

				fit_error_wo = 0
				fit_error_w = 0
				data_purity_stat_wo = 0
				data_purity_stat_w = 0
				purity_wo = 0
				purity_w = 0

				mass_min	= 65
				mass_max	= 105

				sig_mass_min	= 65#80
				sig_mass_max	= 105#100	

				#Finding bin max and min from data file
				bin_min = 1
				bin_max = histData1.GetNbinsX()

				sig_bin_min = histData1.GetXaxis().FindBin(65+0.1) 
				sig_bin_max =  histData1.GetXaxis().FindBin(105-0.1)

				sig_range_min = histData1.GetXaxis().FindBin(80+0.1)
				sig_range_max = histData1.GetXaxis().FindBin(100-0.1)


				print "Signal bin min is :",sig_bin_min
				print "Signal bin max is :",sig_bin_max

				#Added in these weights to get more error bars on the MC plots since we destroyed the statictis by using histBkg2.GetEntries() instead of histBkg2.Integral(sig_bin_min, sig_bin_max)

				print  "HistSignal1.GetEntries():  ",Mllg_eegammaPt					

				w_1= histSignal1.GetSumOfWeights()/histSignal1.GetEntries()
				w_2 = histSignal2.GetSumOfWeights()/histSignal2.GetEntries()
				w_avg = (w_1 + w_2)/2.0
				histSignal1.Scale(1.0/w_avg)
				histSignal2.Scale(1.0/w_avg)
				
				mass= RooRealVar("mass","m_{"+bkg_m[0]+"#gamma} [GeV]", mass_min, mass_max)
				rmass = RooArgList(mass)

				mass.setRange("sig_range", sig_mass_min, sig_mass_max)
				mass.setRange("full_range", mass_min, mass_max)	

				h_d_mass_before = RooDataHist("h_d_mass_before", "h_d_mass_before", rmass, histData1)
				h_d_mass_after = RooDataHist("h_d_mass_after", "h_d_mass_after", rmass, histData2)

				h_mc_mass_sig_before = RooDataHist("h_mc_mass_sig_before", "h_mc_mass_sig_before", rmass,histSignal1)
				h_mc_mass_sig_after = RooDataHist("h_mc_mass_sig_after", "h_mc_mass_sig_after", rmass, histSignal2)

				h_mc_mass_bkg_before = RooDataHist("h_mc_mass_bkg_before", "h_mc_mass_bkg_before", rmass, histBkg1)
				h_mc_mass_bkg_after = RooDataHist("h_mc_mass_bkg_after", "h_mc_mass_bkg_after", rmass, histBkg2)
				

				print "Full data before integral:", histData1.Integral(int(bin_min), int(bin_max))
				print "Signal data before integral:",histData1.Integral(int(sig_bin_min), int(sig_bin_max))
				print "Signal mc before integral:",histSignal1.Integral(int(sig_bin_min), int(sig_bin_max))
				print "Background mc before integral:",histBkg1.Integral(int(sig_bin_min), int(sig_bin_max))
			
				#3rd degree polynomial fit
				p1 = RooRealVar("p1","p1", -10, 10)
				p2 = RooRealVar("p2","p2", -10, 10)
				p3 = RooRealVar("p3","p3", -10, 10)

				#Background Fits
				#pdf_bkg_before = RooPolynomial("pdf_bkg_before","pdf_bkg_before",mass,RooArgList(p1,p2,p3))
				#pdf_bkg_after = RooPolynomial("pdf_bkg_after","pdf_bkg_before",mass,RooArgList(p1,p2,p3))
				
				pdf_bkg_before = RooHistPdf("pdf_bkg_before","pdf_bkg_before", RooArgSet(mass), h_mc_mass_bkg_before)
				pdf_bkg_after =  RooHistPdf("pdf_bkg_after","pdf_bkg_after", RooArgSet(mass), h_mc_mass_bkg_after)
				
				pdf_sig_before =  RooHistPdf("pdf_sig_before","pdf_bkg_before", RooArgSet(mass), h_mc_mass_sig_before)
				pdf_sig_after=  RooHistPdf("pdf_sig_after","pdf_bkg_after", RooArgSet(mass), h_mc_mass_sig_after)

				nd_before = histData1.Integral(int(sig_bin_min), int(sig_bin_max))
				nd_after = histData2.Integral(int(sig_bin_min), int(sig_bin_max))

				#fsig_before = RooRealVar("fsig_before","signal_before", nd_before, 0,2*nd_before)
				fsig_before = RooRealVar("fsig_before","signal_before", nd_before, 0.2*nd_before,2*nd_before)
				fbkg_before= RooRealVar("fbkg_before","bkg_before", 0.05*nd_before, 0, 0.8*nd_before)

				fsig_after= RooRealVar("fsig_after","signal_after", nd_after, 0.2*nd_after,2* nd_after)
				fbkg_after= RooRealVar("fbkg_after","bkg_after", 0.05*nd_after, 0, 0.8*nd_after)


				
				var = RooArgSet(mass)

				#nsig_before = pdf_sig_before.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
				#nbkg_before = pdf_bkg_before.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))

				#nsig_after = pdf_sig_after.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
				#nbkg_after = pdf_bkg_after.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))

				

				sum_before = RooAddPdf()
				sum_after  = RooAddPdf()

				if (bkgTemplate == 1):

					sum_before = RooAddPdf("sum_before", "sum_before",RooArgList(pdf_sig_before, bkg_polpdf_before), RooArgList(fsig_before, fbkg_before)
				)
					sum_after = RooAddPdf("sum_after", "sum_after",RooArgList(pdf_sig_after, bkg_polpdf_after), RooArgList(fsig_after, fbkg_after)
				)
				else:

					sum_before = RooAddPdf("sum_before", "sum_before",RooArgList(pdf_sig_before, pdf_bkg_before), RooArgList(fsig_before, fbkg_before)
				)
					sum_after = RooAddPdf("sum_after", "sum_after",RooArgList(pdf_sig_after, pdf_bkg_after), RooArgList(fsig_after, fbkg_after)
				)



				#sig_after = histData2.Integral(sig_bin_min, sig_bin_max)
				#bkg_after = sig_after/1000.
				#sig_before = histData1.Integral(sig_bin_min, sig_bin_max) 
				#bkg_before = sig_before/1000.
			
				#Do the fit over the entire range	
				if pTbins[nptB] < 120.0 : 	

					print "Nope: I did the fit and this is the first pt bin:", pTbins[nptB]
					print "Nope: I did the fit and this is the second pt bin:", pTbins[nptB+1]

					fitResults_before = sum_before.fitTo(h_d_mass_before, RooFit.SumW2Error(True), RooFit.Save(), RooFit.Range("sig_range"), RooFit.SumCoefRange("sig_range"), RooFit.PrintLevel(-1))
					
					#Chris Suggestions
					nsig_before = pdf_sig_before.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
					nbkg_before = pdf_bkg_before.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
					#sig_before = histData1.Integral(int(sig_range_min), int(sig_range_max)) * nsig_before.getVal() / (nsig_before.getVal() + nbkg_before.getVal())

					sum_before.fixCoefRange("sig_range")

					params_before = RooArgSet(sum_before.getVariables())
					params_before.Print("v")

					print dataFlag[Z]+"Eta bin"+str(e)+" Pt Bin number: "+str(nptB)
					print "number of sig before is", fsig_before.getVal(),"+/-", fsig_before.getError()
					print "number of bkg before is", fbkg_before.getVal(),"+/-", fbkg_before.getError()
					
					#sig_before = fsig_before.getVal()
					sig_before = nsig_before.getVal()

					#Chris Suggestion
					#sig_before = nd_before * nsig_before.getVal() / (nsig_before.getVal() + nbkg_before.getVal())
					bkg_before = fbkg_before.getVal()
					

					#sig_ratio_before = sig_before/(histData1.Integral(int(sig_bin_min), int(sig_bin_max)))
					#bkg_ratio_before = bkg_before/(histData1.Integral(sig_bin_min , sig_bin_max))
					print "Eta bin"+str(e)+" Pt Bin number: "+str(nptB)
					sig_ratio_before = sig_before/(histData1.Integral(int(sig_range_min), int(sig_range_max)))
					bkg_ratio_before = bkg_before/(histData1.Integral(int(sig_range_min) , int(sig_range_max)))

					print "Signal ratio before: ", sig_ratio_before
					print "Background ratio before: ", bkg_ratio_before
					
					fitResults_after = RooFitResult(sum_after.fitTo(h_d_mass_after, RooFit.SumW2Error(True),RooFit.Save(), RooFit.Range("sig_range"), RooFit.SumCoefRange("sig_range"), RooFit.PrintLevel(-1)))
					
					#Chris Suggestion
					nsig_after = pdf_sig_after.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
					nbkg_after = pdf_bkg_after.createIntegral(var,RooFit.Range( int(sig_range_min), int(sig_range_max)))
					#sig_after = histData2.Integral(int(sig_range_min), int(sig_range_max)) * nsig_after.getVal() / (nsig_after.getVal() + nbkg_after.getVal())

					sum_after.fixCoefRange("sig_range")

					params_after = sum_after.getVariables()
					params_after.Print("v")

					#sig_before = fsig_after.getVal()
					sig_after = nsig_after.getVal()
					bkg_after = fbkg_after.getVal()
					
					print "number of sig after is ", sig_after
					print "number of bkg after is ", bkg_after

					#Checking to see which bin is failing
					print "Eta bin number is ="+str(e)+" Bin number is =" +str(nptB)	

					#sig_ratio_after = sig_after/(histData2.Integral(sig_bin_min , sig_bin_max))
					#bkg_ratio_after = bkg_after/(histData2.Integral(sig_bin_min , sig_bin_max))
					print "----------------------------------------------------------------------------------------"
					print "sig_after/(histData2.Integral(sig_range_min , sig_range_max))"
					print "sig_after", sig_after
					print "histData2.Integral(sig_range_min , sig_range_max)", histData2.Integral(sig_range_min , sig_range_max)
					print "----------------------------------------------------------------------------------------"
					sig_ratio_after = sig_after/(histData2.Integral(sig_range_min , sig_range_max))
					bkg_ratio_after = bkg_after/(histData2.Integral(sig_range_min , sig_range_max))
	
					print "Signal ratio after: ", sig_ratio_after
					print "Background ratio after: ", bkg_ratio_after
	
					print "Eta number is ="+str(e)+" Bin number is =" +str(nptB)	
				#data_ent_wo = histData1.Integral(sig_bin_min, sig_bin_max) 
				#data_ent_w = histData2.Integral(sig_bin_min, sig_bin_max) 
				data_ent_wo = histData1.Integral(sig_range_min, sig_range_max) 
				data_ent_w = histData2.Integral(sig_range_min, sig_range_max) 
				
				#mc_ent_wo = histSignal1.Integral(sig_bin_min, sig_bin_max)
				#mc_ent_w = histSignal2.Integral(sig_bin_min, sig_bin_max)
				mc_ent_wo = histSignal1.Integral(sig_range_min, sig_range_max)
				mc_ent_w = histSignal2.Integral(sig_range_min, sig_range_max)

		#data_ent_wo = data entires that does not have the loose photon criteria
		#Data_ent_w = data entries that does fit the loose photon criteria
				myEff_d_Zll.SetBinContent(nptB+1,int( data_ent_wo))
				if (data_ent_w > data_ent_wo):
					print "ERROR: Nope for not corr data: N total events < N passed events, bailing out...."
				myEff_d_Zll_loose.SetBinContent(nptB+1, int(data_ent_w))


				print "Data ent wo", data_ent_wo
				print "Data ent w", data_ent_w
				eff_nocorr_central = myEff_d_Zll.GetBinContent(nptB+1)
				eff_nocor_sys = sqrt(sqrt(data_ent_w)/data_ent_w*(sqrt(data_ent_w)/data_ent_w) + sqrt(data_ent_wo)/data_ent_wo*(sqrt(data_ent_wo)/data_ent_wo))

				myEff_mc_Zll.SetBinContent(nptB+1, int(mc_ent_wo))
				if (mc_ent_w> mc_ent_wo):
					print("ERROR: Nope for mc: N total events < N passed events, setting passed to total ...")
					mc_ent_w = mc_ent_wo

				myEff_mc_Zll_loose.SetBinContent(nptB+1, int(mc_ent_w))
				print "mc_ent_wo", mc_ent_wo
				print "mc_ent_w", mc_ent_w	
				eff_mc_central = myEff_mc_Zll.GetBinContent(nptB+1)
				eff_mc_syst = myEff_mc_Zll.GetBinContent(nptB+1)

				if (bkg_before == 0):
					fit_error_wo = sqrt((sqrt(sig_before)/sig_before)*(sqrt(sig_before)/sig_before) + 0)
				
				else:
					print "sig_before", sig_before
					fit_error_wo = sqrt((sqrt(sig_before)/sig_before)*(sqrt(sig_before)/sig_before) + histData1.Integral(sig_bin_min, sig_bin_max))
				

				if (bkg_after == 0):
					print "Sig after", sig_after
					fit_error_w = sqrt((sqrt(sig_after)/sig_after)*(sqrt(sig_after)/sig_after) + 0 )
				
				else:
					fit_error_w = sqrt((sqrt(sig_after)/sig_after)*(sqrt(sig_after)/sig_after) + (sqrt(bkg_after)/bkg_after)*(sqrt(bkg_after)/bkg_after))

				purity_wo = 1/(1+((bkg_before)/sig_before))
				purity_w = 1/(1+((bkg_after)/sig_after))

				#Signal before should never be less than signal after
				if int(sig_before)<int(sig_after):
					sig_after=sig_before

				myEff_d_Zll_corr.SetBinContent(nptB+1, int(sig_before))


				if (sig_after > sig_before):

					print "Nope: N total events < N passed events, bailing out...\n"

				myEff_d_Zll_corr_loose.SetBinContent(nptB +1 , int(sig_after))
				myEff_d_Zll_corr_loose.Print("all")
				c2 = TCanvas("c2","c2", 147, 22, 800, 600)
				myEff_d_Zll_corr_loose.Draw()
				c2.SaveAs(executable_dir_path+"/photon_events_pass_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_Data_Corrected_Loose_Eta_#"+str(e)+"PtBin_"+str(nptB)+".png")
				


				eff_central= myEff_d_Zll_corr.GetBinContent(nptB+1)

		#		eff_syst = sqrt(myEff_d_Zll_corr.GetEfficiencyErrorLow(nptB)*myEff_d_Zll_corr.GetEfficiencyErrorLow(nptB) + myEff_d_Zll_corr.GetEfficiencyErrorUp(nptB)*myEff_d_Zll_corr.GetEfficiencyErrorUp(nptB))
				eff_syst = 0.0 
				eff_fit_syst = 1/sqrt(1/(fit_error_w*fit_error_w) + 1/(fit_error_wo*fit_error_wo))
				eff_central_alt = (histData2.Integral(sig_bin_min, sig_bin_max) - bkg_after)/(histData1.Integral(sig_bin_min, sig_bin_max) - bkg_before)
				eff_syst_alt = eff_syst

				print "eff data not corr:", eff_nocorr_central
				print "eff mc: ", eff_mc_central
				print "eff_central: ", eff_central
				print "eff_central alt: ", eff_central_alt
				print "purity w/o: ", purity_wo, "+/-", fit_error_wo
				print "purity with: ", purity_w, "+/-", fit_error_w

				if (eff_central < eff_nocorr_central): 
					eff_central = eff_nocorr_central
				if (eff_syst > 0.5): 
					eff_syst = eff_nocorr_syst

				if (bkgTemplate ==1):
					h_before = mass.frame(ROOT.RooFit.Title("Data Before_"+Mllg_data15_17))
					sum_before.plotOn(h_before, ROOT.RooFit.Range("full_range"), RooFit.Components(RooArgSet(pdf_sig_before, bkg_polpdf_before)),RooFit.LineColor(1))
				else:

					c1 = TCanvas("c1","c1", 147, 22, 800, 600)
					c1.SetLogy()
					h_before = mass.frame(ROOT.RooFit.Title("Data Before_"+Mllg_data15_17))
					h_d_mass_before.plotOn(h_before)

					sum_before.plotOn(h_before,ROOT.RooFit.Range("full_range"),RooFit.Name("sum_before"))
					sum_before.plotOn(h_before,ROOT.RooFit.Range("full_range"),RooFit.Components("pdf_sig_before"), RooFit.LineColor(ROOT.kRed),RooFit.Name("pdf_sig_before"))
					sum_before.plotOn(h_before,ROOT.RooFit.Range("full_range"),RooFit.Components("pdf_bkg_before"), RooFit.LineStyle(ROOT.kDashed),RooFit.Name("pdf_bkg_before"))



					h_before.Draw()
					h_before.SetMinimum(0.2)
					h_before.SetMaximum(2e7)

					#Making Legend
					#leg1 = ROOT.TLegend(0.22,0.79,0.485,0.9)
					leg1 = ROOT.TLegend(0.17,0.7,0.4,0.9)
					leg1.AddEntry("","	ATLAS #bf{#bf{internal}}","")
					leg1.AddEntry("","#sqrt{s} = 13 TeV, 139#it{fb}^{-1}","")
					leg1.AddEntry("","Before Loose ID","")
					leg1.AddEntry("",str(etaBins[e][0])+"< |#eta| <"+str(etaBins[e][1]),"")
					leg1.AddEntry("",str(pTbins[nptB])+"< p_{T} <"+str(pTbins[nptB+1]),"")
					leg1.SetBorderSize(0)
					leg1.SetFillColor(0)
					leg1.SetFillStyle(0)
					leg1.SetTextFont(32)
					leg1.SetTextSize(0.035)
					leg1.Draw()

					#Making legend for points
					leg2=ROOT.TLegend(0.71,0.7397035,0.83,0.9093904)
					leg2.SetBorderSize(0)
					leg2.SetTextSize(0.04347826)
					leg2.SetLineColor(1)
					leg2.SetLineStyle(1)
					leg2.SetLineWidth(1)
					leg2.SetFillColor(10)
					leg2.SetFillStyle(1001)

					leg2.AddEntry("h_d_mass_before"," Data","pl")
					leg2.AddEntry(h_before.findObject("pdf_sig_before"),"Signal","l")
					leg2.AddEntry(h_before.findObject("pdf_bkg_before"),"Background","l")
					leg2.AddEntry(h_before.findObject("sum_before"),"Sum","l")
					leg2.Draw()
							
					c1.SaveAs(executable_dir_path+"/bkgfit_before_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"BeforeData_"+Mllg_data15_17+".png")	
					

					c2 = TCanvas("c2","c2", 147, 22, 800, 600)
					c2.SetLogy()
					h_after = mass.frame(ROOT.RooFit.Title("Data After_"+Mllg_data15_17))
					h_d_mass_after.plotOn(h_after)

					sum_after.plotOn(h_after,RooFit.Name("sum_after"))
					sum_after.plotOn(h_after,RooFit.Components("pdf_sig_after"), RooFit.LineColor(ROOT.kRed),RooFit.Name("pdf_sig_after"))
					sum_after.plotOn(h_after,RooFit.Components("pdf_bkg_after"), RooFit.LineStyle(ROOT.kDashed),RooFit.Name("pdf_bkg_after"))
					
					h_after.Draw()
					h_after.SetMinimum(0.2)
					h_after.SetMaximum(2e7)

					#Making Legend
					#leg1 = ROOT.TLegend(0.22,0.79,0.485,0.9)
					leg3 = ROOT.TLegend(0.17,0.7,0.4,0.9)
					leg3.AddEntry("","	ATLAS #bf{#bf{internal}}","")
					leg3.AddEntry("","#sqrt{s} = 13 TeV, 139#it{fb}^{-1}","")
					leg3.AddEntry("","After Loose ID","")
					leg3.AddEntry("",str(etaBins[e][0])+"< |#eta| <"+str(etaBins[e][1]),"")
					leg3.AddEntry("",str(pTbins[nptB])+"< p_{T} <"+str(pTbins[nptB+1]),"")
					leg3.SetBorderSize(0)
					leg3.SetFillColor(0)
					leg3.SetFillStyle(0)
					leg3.SetTextFont(32)
					leg3.SetTextSize(0.035)
					leg3.Draw()

					#Making legend for points
					leg4=ROOT.TLegend(0.71,0.7397035,0.83,0.9093904)
					leg4.SetBorderSize(0)
					leg4.SetTextSize(0.04347826)
					leg4.SetLineColor(1)
					leg4.SetLineStyle(1)
					leg4.SetLineWidth(1)
					leg4.SetFillColor(10)
					leg4.SetFillStyle(1001)

					leg4.AddEntry("h_d_mass_after"," Data","pl")
					leg4.AddEntry("pdf_sig_after"," Signal","l")
					leg4.AddEntry("pdf_bkg_after"," Background","l")
					leg4.AddEntry("sum_after"," Sum","l")
					leg4.Draw()
							
					c2.SaveAs(executable_dir_path+"/bkgfit_after_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"AfterData_"+Mllg_data15_17+".png")	
				
			#Saving Histograms - all photons
			c2 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_d_Zll.Draw("ape")
			c2.SaveAs(executable_dir_path+"/bkgfit_before_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_Data.png")	
			c8 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_mc_Zll.Draw("pe")
			c8.SaveAs(executable_dir_path+"/bkgfit_before_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_MC.png")	
			c9 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_d_Zll_corr.Draw("pe")
			c9.SaveAs(executable_dir_path+"/bkgfit_before_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_Data_Corrected.png")
			#Saving Histograms -  photons pass loose criteria
			c2 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_d_Zll_loose.Draw()
			c2.SaveAs(executable_dir_path+"/bkgfit_after_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_Data_Loose.png")
			myEff_mc_Zll_loose.Draw()
			c2.SaveAs(executable_dir_path+"/bkgfit_after_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_MC_Loose.png")
			myEff_d_Zll_corr_loose.Draw()
			c2.SaveAs(executable_dir_path+"/bkgfit_after_loose_criteria/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Zll_Data_Corrected_Loose.png")

			## Creating Efficiency and Scale Factor Plots
			#MC Effiency 
			c5 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_mc_Zll_loose.Sumw2()
			myEff_mc_Zll.Sumw2()

			g_efficiency_mc = ROOT.TGraphAsymmErrors(myEff_mc_Zll_loose, myEff_mc_Zll, "cl=0.683 b(1,1) mode")

			g_efficiency_mc.SetName(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_MC")
			g_efficiency_mc.SetTitle(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_MC")			
			g_efficiency_mc.Draw("ape")
			c5.SaveAs(executable_dir_path+"/Zll_efficiency/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_MC.png")

			#Making efficiency for Data without correction to check for problems
			c6 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_d_Zll_loose.Sumw2()
			myEff_d_Zll.Sumw2()
			data_eff_check=ROOT.TGraphAsymmErrors(myEff_d_Zll_loose, myEff_d_Zll, "cl=0.683 b(1,1) mode")

			data_eff_check.SetName(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_data_noBkgFit")
			data_eff_check.SetTitle(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_data_noBkgFit")			
			data_eff_check.Draw("ape")
			c6.SaveAs(executable_dir_path+"/Eff_check/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_data_noBkgFit.png")

			#DATA With Correction
			c7 = TCanvas("c2","c2", 147, 22, 800, 600)
			myEff_d_Zll_corr_loose.Sumw2()
			myEff_d_Zll_corr.Sumw2()
			g_efficiency_d = ROOT.TGraphAsymmErrors(myEff_d_Zll_corr_loose, myEff_d_Zll_corr, "cl=0.683 b(1,1) mode")

			g_efficiency_d.SetName(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_Data")
			g_efficiency_d.SetTitle(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_Data")			
			g_efficiency_d.Draw("ape")
			c7.SaveAs(executable_dir_path+"/Zll_efficiency/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"Eff_Zll_Data.png")
		
			#Calculating the scale factors	

			c20 = TCanvas("c2","c2", 147, 22, 800, 600)
			h_sf = myEff_d_Zll_corr_loose.Clone("SFs")
			h_sf.Divide(myEff_d_Zll_corr)
			h_sf.Multiply(myEff_mc_Zll)
			h_sf.Divide(myEff_mc_Zll_loose)

			h_sf.SetName(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"SF_Zll")
			h_sf.SetTitle(convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"SF_Zll")
			h_sf.Draw("pe")
			c20.SaveAs(executable_dir_path+"/Zll_scale_factors/"+convFlag[conV]+"Eta_Cut_"+str(e)+plot_name[Z]+"SF_Zll.png")

			

			ofile.cd()
			g_efficiency_mc.Write()
			g_efficiency_d.Write()
			data_eff_check.Write()
			myEff_mc_Zll_loose.Write()			
			myEff_mc_Zll.Write()
			myEff_d_Zll_corr.Write()
			myEff_d_Zll_corr_loose.Write()
			h_sf.Write()


ofile.Close()

