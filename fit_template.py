import ROOT
import array
import templatefit

ROOT.gROOT.SetBatch(True)

# channels = ["ll", "ee", "mumu"]
channels = ["ll"]

# convType = [0, 1, 2]
convType = [2]

etaRanges = [ [0.0,0.6], [0.06,0.8], [0.8,1.37], [1.52,1.81], [1.81,2.01], [2.01,2.37]]
# etaRanges = [ [0.0,0.6] ]
etaBins = len(etaRanges)

pTboundaries = array.array('d', [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 100])
# pTboundaries = array.array('d', [10, 15, 20])
pTbins = len(pTboundaries)-1

ofile = ROOT.TFile.Open("efficiency.root", "RECREATE")

for chan in channels:

  path = "templates/"
  dfile = ROOT.TFile.Open(path+"data_"+chan+".root", "READ")
  gfile = ROOT.TFile.Open(path+"Sh_"+chan+"g.root", "READ")
  jfile = ROOT.TFile.Open(path+"Py_"+chan+"j.root", "READ")

  for c in convType:
    for e in range(etaBins):
      lgam  = []
      lgamL = []

      ldata  = []
      ldataL = []
      for p in range(pTbins):
        print ""
        print "******** Fitting c = "+str(c)+", eta = ["+str(etaRanges[e][0])+","+str(etaRanges[e][1])+"], pT = ["+str(pTboundaries[p])+","+str(pTboundaries[p+1])+"] **********"
        print ""

        # Get histograms for template fit
        postfix = "_conv"+str(c)+"_eta"+str(e)+"_pt"+str(p)
        dhist = dfile.Get("mllg_data"+postfix)
        ghist = gfile.Get("mllg_gam"+postfix)
        jhist = jfile.Get("mllg_jet"+postfix)

        dhistL = dfile.Get("mllg_dataL"+postfix)
        ghistL = gfile.Get("mllg_gamL"+postfix)
        jhistL = jfile.Get("mllg_jetL"+postfix)

        # Get denominator from template fit
        postfix = chan+"_conv"+str(c)+"_eta"+str(e)+"_pt"+str(p)
        ndata = templatefit.fitData(dhist, ghist, bkg = jhist, nlow = 80, nhigh = 100, plotname = "plots/fit_"+postfix)
        # ndata = templatefit.fitData(dhist, ghist, nlow = 80, nhigh = 100, plotname = "plots/fit_"+postfix)
        ldata.append(int(ndata))

        # Get numerator from template fit
        ndataL = templatefit.fitData(dhistL, ghistL, bkg = jhistL, nlow = 80, nhigh = 100, plotname = "plots/fitL_"+postfix)
        # ndataL = templatefit.fitData(dhistL, ghistL, nlow = 80, nhigh = 100, plotname = "plots/fitL_"+postfix)
        ldataL.append(min(int(ndata),int(ndataL)))

        # Get MC eff numerator and denominator from integral
        ngamL, ngam = templatefit.scaleIntegralToPoissonError(ghistL, ghist, nlow = 80, nhigh = 100)
        lgam.append(ngam)
        lgamL.append(ngamL)

      print ""
      print "******* Efficiency for c = "+str(c)+", eta = ["+str(etaRanges[e][0])+","+str(etaRanges[e][1])+"] *********"
      print ""

      # Make and save efficiency histograms
      postfix = "_conv"+str(c)+"_eta"+str(e)
      ofile.cd()

      print "mc loose:",lgamL
      print "mc all:",lgam

      print "data loose:",ldataL
      print "data all:",ldata

      # eff = templatefit.getEffNadezda(lgamL, lgam, pTboundaries, "mceff_"+chan+postfix)
      eff = templatefit.getEff(lgamL, lgam, pTboundaries, "mceff_"+chan+postfix)
      eff.Write()

      # eff = templatefit.getEffNadezda(ldataL, ldata, pTboundaries, "deff_"+chan+postfix)
      eff = templatefit.getEff(ldataL, ldata, pTboundaries, "deff_"+chan+postfix)
      eff.Write()

