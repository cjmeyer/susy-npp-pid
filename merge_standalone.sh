#!/bin/bash

# Where am I?
export NODEDIR="$PWD"
echo "Running on: `uname -a`"
echo "Work area : $NODEDIR"
echo "Start time: `date`"
echo

# Make temp directory needed before setup
mkdir -p /tmp/cjmeyer/.alrb

# Setup environment
echo "Setting up environment"
source setup.sh 2>&1
echo

echo "Changing back to work area: $NODEDIR"
cd $NODEDIR
echo

echo "Contents of work area:"
ls
echo

# Start merging
filelist=""
for sample in "$@"; do
  filelist="$filelist $sample"
done

echo xAODMerge output.root $filelist
xAODMerge output.root $filelist 2>&1
echo

echo cp output.root ${OUTPUT}
cp output.root ${OUTPUT} 2>&1
echo

# # Clean up staged files after merging, assuming it finished succesfully
# temp=${OUTPUT:7:${#OUTPUT}} # strip root:// from file
# server=${temp%%/*}
# path=${temp#*/}
# resp=$(xrd ${server} existfile ${path})
# if [[ ${resp} == *exist* ]]; then
#   for file in ${filelist}; do
#     temp=${file:7:${#file}} # strip root:// from file
#     server=${temp%%/*}
#     path=${temp#*/}
#     # echo xrd $server rm $path
#     # xrd $server rm $path 2>&1
#   done
#   echo
# else
#   echo "Couldn't find ${OUTPUT} on xrootd server??"
#   echo "Keeping subjob files for now."
#   echo
# fi

echo "Contents of work area:"
ls
echo

# Done!
echo "Done with job, exiting!"
rm -rf x509_proxy output.root setup.sh 2>&1
echo

echo "End time: `date`"
