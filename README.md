Initial README file for summer project

To install:
```
source setup_cvmfs.sh
mkdir build
cd build
cmake ../source
make -jN # where N is how many cores you want to use
x86_64-centos7-gcc8-opt/setup.sh
```

Going forward, you will only need to:
```
source setup_cvmfs.sh
```
