import ROOT
from math import sqrt

def getGraphMeanRMS(replicas, name):
  if len(replicas) < 1:
    raise Exception('boothelp.py :: getMeanRMS() :: len(replicas) < 1')

  graph = replicas[0].Clone()
  graph.SetName(name)
  graph.SetTitle(name)

  npoints = graph.GetN()
  nreps = len(replicas)

  val = [ 0 ] * (npoints)
  val2 = [ 0 ] * (npoints)

  for rep in replicas:
    for p in range(npoints):
      y = rep.GetPointY(p)
      val[p] += y
      val2[p] += y*y

  for p in range(npoints):
    graph.SetPointY(p, val[p]/nreps)
    graph.SetPointEYhigh(p, sqrt(abs(val2[p]/nreps - val[p]*val[p]/nreps/nreps)))
    graph.SetPointEYlow(p, sqrt(abs(val2[p]/nreps - val[p]*val[p]/nreps/nreps)))

  return graph
