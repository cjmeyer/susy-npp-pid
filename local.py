import os

def getFilesInDir(path):
  if os.path.exists(path) and os.path.isfile(path):
    return [path]
  if os.path.exists(path) and os.path.isdir(path):
    return [path + "/" + f for f in os.listdir(path) if os.path.isfile(path + "/" + f) and f.find("root") != -1 ]
  print "  ERROR: path not found on local file system"
  return []
